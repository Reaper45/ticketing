<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 11/20/17
 * Time: 3:44 PM
 */

use Illuminate\Database\Capsule\Manager;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Middleware\Cors;
use \Controllers\ApiController;

require '../vendor/autoload.php';

// Import config variables
$config = require_once 'includes/config.php';

$app = new \Slim\App(["settings" => $config]);

// Enable Cors
$app->add(new Cors());
$container = $app->getContainer();

// Bootstrap Eloquent
$capsule = new Manager;
$capsule->addConnection($container->get('settings')['db']);

$capsule->setAsGlobal();
$capsule->bootEloquent();

// Define routes Here
$app->group('/api', function ()  use ($app){
    // Register New User
    $app->post('/register', ApiController::class. ':register');

    // Login User
    $app->post('/login', ApiController::class. ':login');

    // Login if user still exists
    $app->post('/auto-login', ApiController::class. ':login_using_id');

    // Create New Event
    $app->post('/events/new', ApiController::class. ':new_event');

    // All Events
    $app->get('/events/all', ApiController::class. ':all_events');

    // Get Event
    $app->get('/events/{id}', ApiController::class. ':get_event');

    // Initiate ticket paymet
    $app->post('/tickets/pay', ApiController::class. ':pay_ticket');

    // Authenticate app
    $app->get('/auth_app', ApiController::class. ':authApp');

    // Confirmation url
    $app->get('/confirmation', ApiController::class. ':confirmation');

    // Valiadation url
    $app->get('/validation_url', ApiController::class. ':validation_url');

});

$app->run();