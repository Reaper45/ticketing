<?php
const APP_ENVIRONMENT = 'production';

$db = array(
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'sph-tickets',
    'username' => 'root',
    'password' => 'password',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
);

$heroku_db = array(
    'driver' => 'pgsql',
    'host' => 'ec2-54-243-54-6.compute-1.amazonaws.com',
    'database' => 'dag7chguld4drd',
    'username' => 'brqaawhhfpvnew',
    'password' => 'ab7cabb12b2a998e66447cd0455f3da7c0348fbe4c2c9a544975b9c0bae5e8db',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
    'port' => 5432
);

return array(
    'displayErrorDetails' => true,
    'addContentLengthHeader' => false,
    'environment' => 'production',
    'db' => APP_ENVIRONMENT == 'production' ? $heroku_db : $db,
    'determineRouteBeforeAppMiddleware' => true,
    'server_name' => $_SERVER['SERVER_NAME'],
    'host_name' => $_SERVER['HTTP_HOST'],
    'document_root' => $_SERVER['DOCUMENT_ROOT'],
    'protocol' => empty($_SERVER['HTTPS']) ? 'http' :'https',
    'm_pesa' => array(
        'key'=> '2XMaBNi0Ztorj8vVFefc61HlI2EnTZ9f',
        'secret'=> 'jjphJGrgjIV3IGtT'
    )
);