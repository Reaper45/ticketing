<?php
/**
 * Created by PhpStorm.
 * User: joram
 * Date: 11/25/17
 * Time: 2:52 PM
 */

namespace Model;


use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';

    protected $primaryKey = 'id';

    public function user ()
    {
        return $this->belongsTo(User::class);
    }
}