<?php
/**
 * Created by PhpStorm.
 * User: joram
 * Date: 11/25/17
 * Time: 2:51 PM
 */

namespace Model;

use Illuminate\Database\Eloquent\Model as BaseModel;

class User extends BaseModel
{
    protected $table = 'users';

    protected $primaryKey = 'id';

    public function setPasswordAttribute ($password)
    {
        $this->attributes['password'] = password_hash($password, PASSWORD_BCRYPT);
    }

    public function events ()
    {
        return $this->hasMany(Event::class);
    }
}