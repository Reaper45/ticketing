<?php
/**
 * Created by PhpStorm.
 * User: joram
 * Date: 11/26/17
 * Time: 10:36 AM
 */

namespace Controllers;

use Carbon\Carbon;
use Classes\SendMoney;
use Model\User;
use Model\Event;
use Ramsey\Uuid\Uuid;
use Slim\Http\Request;
use Slim\Http\Response;


class ApiController
{
   const MPESA_BASE_URL = 'https://sandbox.safaricom.co.ke/mpesa/c2b/v1';

    /**
     * Register new Organisers account
     *
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function register(Request $request, Response $response): Response
    {
        $data = $request->getParsedBody();
        $user = new User();

        if (User::where('email', $data['email'])->first()) {
            return $response->withJson($this->api_response('Error', null, 'Email already taken!'), 200);
        }
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->phone_number = $data['phone_number'];
        $user->password = $data['password'];

        if ($user->save()) {
            return $response->withJson($this->api_response('Success', $user, 'Account set up successful!'), 200);
        }
        return $response->withJson($this->api_response('Error', $user, 'Unknown error occurred! Retry!'), 200);
    }

    /**
     * Login user
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function login(Request $request, Response $response, $args): Response
    {
        $data = $request->getParsedBody();
        $user = User::where('email', $data['email'])->first();
        if ($user != null && password_verify($data['password'], $user->password)) {
            return $response->withJson($this->api_response('Success', $user, "Welcome back {$user->name}!"), 200);
        }
        return $response->withJson($this->api_response('Error', null, 'Invalid Email/Password!'), 200);
    }

    public function login_using_id(Request $request, Response $response)
    {
        $id = $request->getParsedBody('id');
        $user = User::find($id)->first();
        if ($user->count() > 0) {
            return $response->withJson($this->api_response('Success', $user, "Logged in as {$user->name}!"),200);
        }
        return $response->withJson($this->api_response('Info', null, "Looks like you've been logged out!"), 200);
    }

    /**
     * Add new event
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     * @internal param $args
     */
    public function new_event(Request $request, Response $response): Response
    {
        $data = $request->getParsedBody();
        $user = User::find($data['user_id']);
        if (!$user) {
            return $response->withJson($this->api_response('Error', null, 'Make sure you are logged in as organiser!'), 401);
        }

        $event = new Event();
        $event->title = $data['title'];
        $event->location = $data['location'];
        $event->description = $data['description'];
        $event->ticket_charges = $data['ticket_charges'];
        $event->image_url = $data['image'];
        $event->event_date = new Carbon($data['event_date']);

        if ($user->events()->save($event)) {
            $event = Event::find($event->id);
            return $response->withJson($this->api_response('Success', $event, 'New event added successful!'), 200);
        }
        return $response->withJson($this->api_response('Error', null, 'Unknown error occurred! Retry!'), 200);
    }

    /**
     * Get single event with {id}
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function get_event(Request $request, Response $response, $args)
    {
        return $response;
    }

    /**
     * Fetch all events
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     * @internal param $args
     */
    public function all_events(Request $request, Response $response): Response
    {
        $events = Event::all();
        foreach ($events as $event) {
            $data_time = explode(' ', $event->event_date);
            $event->date = $data_time[0];
            $event->time = $data_time[1];
        }
        return $response->withJson($this->api_response('Success', ['events' => $events], null), 200);
    }

    /**
     * Decode and move base64 image
     *
     * @param string $data
     * @return string
     */
    public function upload_bas64_image(string $data): string
    {
        global $config;
        $image_data = explode(',', $data);

        $extention = explode(';', explode('/', $image_data[0])[1])[0];
        $filename = '/static/images/' . Uuid::uuid1()->toString() . '.' . $extention;

        $upload_to_path = $config['document_root'] . $filename;
        if (file_put_contents($upload_to_path, base64_decode($image_data[1]))) {
            return $config['protocol'] . '://' . $config['host_name'] . $filename;
        }
        return false;
    }

    /**
     * Callback function form Lipisha payments
     *
     * @param Request $request
     * @param Response $response
     * @return static
     */
    public function pay_ticket (Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        $access_token = 'Authorization: Bearer '.$data['access_token'];
        $url = 'https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
        $payload = [
            'BusinessShortCode' => ' ',
            'Password' => '',
            'Timestamp' => time(),
            'TransactionType' => 'CustomerPayBillOnline',
            'Amount"' => $data['amount'],
            'PartyA' => ' ',
            'PartyB' => ' ',
            'PhoneNumber' => $data['phone_number'],
            'CallBackURL' => 'https://ip_address:port/callback',
            'AccountReference' => ' ',
            'TransactionDesc' => ' '
        ];
        $payload = json_encode($payload);
        $options = array('Content-Type:application/json', $access_token);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $options);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);

        $curl_response = curl_exec($curl);
        curl_close($curl);

        if ($data['status'] == 'SUCCESS')
            return $response->withJson($this->api_response('success', $curl_response, "Almost There!"));
        return $response->withJson($this->api_response('Error',  $curl_response, 'Error'));
    }
    /**
     * Format response data
     *
     * @param $status
     * @param $payload
     * @param $message
     * @return array
     */
    public function api_response($status, $payload, $message): array
    {
        return array('status' => $status, 'payload' => $payload, 'message' => $message);
    }

    public function authApp (Request $request, Response $response)
    {
        global $config;
        $url = "https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials";
        $credentials = base64_encode($config['mpesa']['key'].":".$config['mpesa']['secret']);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Authorization: Basic '.$credentials]); //setting a custom header
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $curl_response = curl_exec($curl);
        curl_close($curl);

        // return array("data" =>json_decode($curl_response));
    }

    public function register_url (Request $request, Response $response): Response
    {

    }

    public function connfirmation (Request $request, Response $response): Response
    {
        return $response->withStatus(200);
    }

    public function validation_url (Request $request, Response $response): Response
    {
        return $response->withStatus(200);
    }


    /**
     * @param $name
     * @param $arguments
     * @return string
     */
    public function __call($name, $arguments): string
    {
        return "Call to undefined method {$name} whit arguments {$arguments}";
    }

}