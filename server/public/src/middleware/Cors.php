<?php
/**
 * Created by PhpStorm.
 * User: joram
 * Date: 11/25/17
 * Time: 3:37 PM
 */

namespace Middleware;

use Slim\Http\Request;
use Slim\Http\Response;

class Cors
{
    public function __invoke(Request $request, Response $response, $next) {

        /** @var $response Response */
        $response = $next($request, $response);

        return $response->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');

    }
}