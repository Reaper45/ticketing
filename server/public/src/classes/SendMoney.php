<?php
/**
 * Created by PhpStorm.
 * User: joram
 * Date: 12/1/17
 * Time: 6:56 PM
 */

namespace Classes;


class SendMoney
{
    private $username = "sandbox";
    private $apiKey   = "0b70993fc3c2b72307c94f1f1409cd945499a59705c2ad3c2598282483beaa03";

    /**
     * @var AfricasTalkingGateway
     */
    private $gateway;

    /**
     * Specify the name of your Africa's Talking payment product
     * @var string
     */
    public $productName  = "SPH-Tickets";

    /**
     * The 3-Letter ISO currency code for the checkout amount
     * @var string
     */
    public $currencyCode = "KES";

    /**
     * @var null
     */
    public $errors = null;

    /**
     * @var null
     */
    public $results = null;

    /**
     * SendMoney constructor.
     */
    public function __construct()
    {
        $this->gateway =  new AfricasTalkingGateway($this->username, $this->apiKey, 'sandbox');
    }

    public function pay($phoneNumber, $amount)
    {
//        return [$phoneNumber, $amount, $metadata];
        try {
            // Initiate the checkout. If successful, you will get back a transactionId
            $transactionId = $this->gateway->initiateMobilePaymentCheckout($this->productName,
                $phoneNumber,
                $this->currencyCode,
                $amount);
            $this->results = $transactionId;
        }
        catch(AfricasTalkingGatewayException $e){
            $this->errors = 'Error! '.$e->getMessage();
        }
    }

    public function fails ()
    {
        return $this->errors != null;
    }
}