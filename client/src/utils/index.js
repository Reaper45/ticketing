import axios from 'axios'
import querystring from 'querystring'

const setToken = (token, expiresIn) => {
  localStorage.setItem('saf_access_token', token)
  let exp = Date.now() + parseInt(expiresIn)
  localStorage.setItem('saf_token_exp', exp)
}

export const getAccessToken = () => {
  axios.get(process.env.API_BASE_URL + '/auth_app')
    .then(data => {
      let token = data.data
      setToken(token.access_token, token.expires_in)
    })
    .catch(err => {
      console.log(err)
    })
}

export const isTokenExpired = () => {
  let exp = parseInt(localStorage.getItem('saf_token_exp'))
  return Date.now() >= exp
}

export const makePayment = (payload) => {
  if (isTokenExpired) {
    getAccessToken()
  }
  return axios.post(process.env.API_BASE_URL + '/tickets/pay', querystring.stringify({...payload}))
}
