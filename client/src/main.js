// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App'
import VueResource from 'vue-resource'
import router from './router'
import './stylus/main.styl'
import { store } from './store'
import { getAccessToken } from './utils'

/* Import Components */
import SearchBar from './components/partials/SearchBar.vue'
import BuyTicket from './components/Events/BuyTicket.vue'
import CreateEvent from './components/Events/CreateEvent.vue'
import Loader from './components/partials/Loader.vue'
import Messages from './components/partials/Messages.vue'

Vue.use(Vuetify)
Vue.use(VueResource)
Vue.config.productionTip = process.env.NODE_ENV === 'development'

/* Register Components */
Vue.component('search-bar', SearchBar)
Vue.component('buy-ticket', BuyTicket)
Vue.component('create-event', CreateEvent)
Vue.component('loader', Loader)
Vue.component('alert-messages', Messages)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  VueResource,
  render: h => h(App),
  created () {
    getAccessToken()
    // Check if user alread loged in
    const user = sessionStorage.getItem('user')
    if (user !== null) {
      this.$store.dispatch('autoLogin', JSON.parse(user))
    }
    this.$store.dispatch('loadEvents')
  }
})
