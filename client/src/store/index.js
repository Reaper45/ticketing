import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import {makePayment} from '../utils'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    loading: false,
    user: null,
    alert: null,
    events: [],
    passDialog: false
  },
  mutations: {
    setLoading (state, payload) {
      state.loading = payload
    },
    setAlert (state, payload) {
      state.alert = payload
    },
    clearAlert (state) {
      state.alert = null
    },
    setUser (state, payload) {
      state.user = payload
    },
    createEvent (state, payload) {
      state.events = [...state.events, payload]
    },
    setLoadedEvents (state, payload) {
      state.events = [ ...payload ]
    },
    controllPasswordDialog (state, payload) {
      state.passDialog = payload
    }
  },
  actions: {
    onRegisterUser ({commit}, payload) {
      commit('setLoading', true)
      commit('clearAlert')
      axios.post(process.env.API_BASE_URL + '/register', payload)
        .then((response) => {
          if (response.data.payload !== null && response.data.status === 'Success') {
            const user = { id: response.data.payload.id, events: [] }
            sessionStorage.setItem('user', JSON.stringify(user))
            commit('setUser', user)
          }
          commit('setAlert', { status: response.data.status, text: response.data.message })
          commit('setLoading', false)
        })
        .catch((error) => {
          console.log(error)
          commit('setAlert', { status: 'Error', text: 'Internal server error. Contact our team for support!' })
          commit('setLoading', false)
        })
    },
    onLoginUser ({commit}, payload) {
      commit('setLoading', true)
      commit('clearAlert')
      axios.post(process.env.API_BASE_URL + '/login', payload)
        .then((response) => {
          if (response.data.payload !== null && response.data.status === 'Success') {
            const user = { id: response.data.payload.id, events: [] }
            sessionStorage.setItem('user', JSON.stringify(user))
            commit('setUser', user)
          }
          commit('setAlert', { status: response.data.status, text: response.data.message })
          commit('setLoading', false)
        })
        .catch((error) => {
          console.log(error)
          commit('setAlert', { status: 'Error', text: 'Internal server error. Contact our team for support!' })
          commit('setLoading', false)
        })
    },
    autoLogin ({commit}, payload) {
      commit('clearAlert')
      var userId = payload.id
      axios.post(process.env.API_BASE_URL + '/auto-login', { id: userId })
        .then((response) => {
          var user = response.data.payload ? { id: response.data.payload.id, events: [] } : null
          if (user === null && (sessionStorage.getItem('user') === 'undefined' || sessionStorage.getItem('user') === null)) {
            commit('setAlert', { status: response.data.status, text: response.data.message })
            sessionStorage.removeItem('user')
          } else {
            commit('setAlert', { status: response.data.status, text: response.data.message })
            commit('setUser', user)
          }
        })
        .catch((error) => {
          console.log(error.response.data)
          commit('setAlert', { status: error.response.data.status, text: error.response.data.message })
        })
    },
    onLogout ({commit}) {
      sessionStorage.removeItem('user')
      commit('setUser', null)
    },
    onCreateNewEvent ({commit, state}, payload) {
      commit('setLoading', true)
      commit('clearAlert')
      payload = { ...payload, user_id: state.user.id }
      axios.post(process.env.API_BASE_URL + '/events/new', payload)
        .then((response) => {
          if (response.data.payload) {
            const event = response.data.payload
            console.log(event)
            commit('createEvent', event)
            commit('setAlert', { status: response.data.status, text: response.data.message })
          }
          commit('setLoading', false)
        })
        .catch((error) => {
          var message = error.response.data.message
          console.log('payload')
          if (message !== 'undefined') {
            commit('setAlert', { status: 'Error', text: message })
          } else {
            console.log(error)
            commit('setAlert', { status: 'Error', text: 'Internal server error. Contact our team for support!' })
          }
          commit('setLoading', false)
        })
    },
    loadEvents ({commit}) {
      commit('setLoading', true)
      commit('clearAlert')
      axios.get(process.env.API_BASE_URL + '/events/all')
        .then((response) => {
          var message = response.data.message
          const events = []
          var obj = response.data.payload.events
          if (message) {
            commit('setAlert', { status: response.data.status, text: message })
          }
          for (let key in obj) {
            events.push({
              id: obj[key].id,
              title: obj[key].title,
              description: obj[key].description,
              src: obj[key].image_url,
              date: obj[key].date,
              time: obj[key].time,
              location: obj[key].location,
              charges: obj[key].ticket_charges,
              userId: obj[key].user_id
            })
          }
          commit('setLoadedEvents', events)
          commit('setLoading', false)
        })
        .catch((error) => {
          console.log(error)
          commit('setAlert', { status: 'Error', text: 'Internal server error. Contact our team for support!' })
          commit('setLoading', false)
        })
    },
    onPayForTickets ({commit, state}, payload) {
      commit('setLoading', true)
      commit('clearAlert')
      const data = { ...payload, userId: state.user.id }
      makePayment(data)
        .then((response) => {
          console.log(response)
          if (response.data.status.status === 'SUCCESS') {
            commit('setAlert', { status: 'Info', text: 'Processing ticket. Check your phone to complete!' })
          }
          commit('setAlert', { status: 'Info', text: 'Could not complete process! ' + response.data.status.status_description })
          commit('setLoading', false)
        })
        .catch((error) => {
          console.log(error)
          commit('setAlert', { status: 'Error', text: 'Internal server error. Contact our team for support!' })
          commit('setLoading', false)
        })
    }
  },
  getters: {
    loading (state) {
      return state.loading
    },
    user (state) {
      return state.user
    },
    alert (state) {
      return state.alert
    },
    loadedEvents (state) {
      return state.events
    },
    loadEvent (state) {
      return (eventId) => {
        return state.events.find((event) => {
          return event.id === eventId
        })
      }
    },
    passwordDialog (state) {
      return state.passDialog
    }
  }
})
